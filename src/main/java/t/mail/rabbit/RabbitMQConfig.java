package t.mail.rabbit;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.List;

@Configuration
public class RabbitMQConfig {


    @Bean
    public Queue registrationQueue() {
        return new Queue("registrationQueue", false);
    }

    @Bean
    public Queue uploadQueue() {
        return new Queue("uploadQueue", false);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange("exchange1", false,false);
    }

    @Bean
    public Binding registrationBinding(Queue registrationQueue, TopicExchange exchange) {
        return BindingBuilder.bind(registrationQueue).to(exchange).with("upload.key");
    }

    @Bean
    public Binding uploadBinding(Queue uploadQueue, TopicExchange exchange) {
        return BindingBuilder.bind(uploadQueue).to(exchange).with("registration.key");
    }

    @Bean
    public RabbitListenerContainerFactory<?> rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(simpleMessageConverter());
        return factory;
    }

//    @Bean
//    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
//        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
//        rabbitTemplate.setMessageConverter(jsonMessageConverter());
//        return rabbitTemplate;
//    }
//
//    @Bean
//    public MessageConverter jsonMessageConverter() {
//        Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter();
//        DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
//        typeMapper.setIdClassMapping(new HashMap<String, Class<?>>() {{
//            put("java.util.HashMap", HashMap.class);
//            put("java.lang.String", String.class);
//        }});
//        converter.setJavaTypeMapper(typeMapper);
//        return converter;
//    }

    @Bean
    public SimpleMessageConverter simpleMessageConverter() {
        SimpleMessageConverter converter = new SimpleMessageConverter();
        converter.setAllowedListPatterns(List.of("src.main.java.t.mail.*", "java.util.HashMap","java.lang.String"));
        return converter;
    }



}
