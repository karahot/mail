package t.mail.service;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MailServiceImpl implements MailService{
    private final JavaMailSender mailSender;

    public MailServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendMailWhenAddUser(String email){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setFrom("SendSendovich@outlook.com");
        message.setText("Hi from core");
        message.setSubject("U add");
        mailSender.send(message);
    }

    @Override
    public void sendMailWhenUpload(Map<String, String> emailAndSize) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emailAndSize.get("email"));
        message.setFrom("SendSendovich@outlook.com");
        message.setText("size: " + emailAndSize.get("size"));
        message.setSubject("Upload");
        mailSender.send(message);
    }

}
