package t.mail.service;

import java.util.Map;

public interface MailService {
    void sendMailWhenAddUser(String email);

    void sendMailWhenUpload(Map<String, String> emailAndSize);
}
