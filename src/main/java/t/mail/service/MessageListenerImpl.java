package t.mail.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MessageListenerImpl implements MessageListener {

    private final MailService mailService;

    public MessageListenerImpl(MailService mailService) {
        this.mailService = mailService;
    }

    @RabbitListener(queues = "registrationQueue")
    public void handleUserRegistration(String email) {
        mailService.sendMailWhenAddUser(email);
    }

    @RabbitListener(queues = "uploadQueue")
    public void handleImageUpload(Map<String,String> emailAndSize) {
        mailService.sendMailWhenUpload(emailAndSize);
    }
}
